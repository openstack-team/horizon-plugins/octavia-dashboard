#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3

override_dh_auto_clean:
	python3 setup.py clean
	rm -rf build .stestr *.egg-info .coverage
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	echo "Do nothing..."

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	for i in $(PYTHON3S) ; do \
		python$$i setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp ; \
	done

	# Move config files to standard location
	install -d -m 755 $(CURDIR)/debian/tmp/etc/openstack-dashboard/local_settings.d
	install -d -m 755 $(CURDIR)/debian/tmp/etc/openstack-dashboard/policy

	# Generate missing policy file
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/octavia_dashboard/conf/octavia_policy.json \
		--format json \
		--namespace octavia

	# Copy policy file to openstack-dashboard policy location
	cp $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/octavia_dashboard/conf/octavia_policy.json $(CURDIR)/debian/tmp/etc/openstack-dashboard/policy/octavia_policy.json

	# Copy config enabled to openstack-dashboard enabled location
	mkdir -p $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/enabled
	cp $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/octavia_dashboard/enabled/_[0-9]*.py $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/enabled

	# Copy local_settings.d to openstack-dashboard local_settings.d location
	cp $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/octavia_dashboard/local_settings.d/_[0-9]*.py $(CURDIR)/debian/tmp/etc/openstack-dashboard/local_settings.d/

	dh_install
	dh_missing --fail-missing

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	for i in $(PYTHON3S) ; do \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages \
			python$$i $(CURDIR)/manage.py test octavia_dashboard -v 2 --settings=octavia_dashboard.tests.settings ; \
	done
	## Delete __pycache__
	find . -name __pycache__ -prune -exec rm -rf {} +;
endif
